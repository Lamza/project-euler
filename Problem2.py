"""
@author = zagi
created on = 7/12/17
File names = Problem2.py
Desc = Even Fibonacci numbers
"""

if __name__ == '__main__':
    tempat = []
    a , b = 0, 1
    next = 0

    while True:
        next = a+b
        if next > 4e6:
            break
        a = b
        b = next
        if (b%2 == 0):
            tempat.append(b)

    print tempat

    print sum(tempat)