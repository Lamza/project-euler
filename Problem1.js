/*
@author = zagi
created on = 7/12/17
File names = Problem1.js
Desc = Multiples of 3 and 5
*/

var result = 0;
for (let i=0; i<1000; i++) {
	if (i%3 == 0 || i%5 == 0){
		result += i
	}
}

console.log(result);
