/*
@author = zagi
created on = 7/12/17
File names = Problem2.js
Desc = Even Fibonacci numbers
*/

var tempat = [];
var lanjut = 0;
var a = 0;
var b = 1;
var jumlah = 0;


while(true){
  lanjut = a +b;
  if (lanjut > 4e6)
    break
  a = b;
  b = lanjut;
  if (b%2 == 0)
    // tempat.push(b)
    jumlah += b;

}

console.log(jumlah);
